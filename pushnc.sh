#!/bin/bash
##################################################################################################
##
##   pushnc FILE           ==> NEXTCLOUD 
##   pullnc NEXTCLOUD LINK ==> FILE
##
##      Kubernetes 
##      -------------------------------
##      Author:         mad.murdock
##      Version:        0.11.1
##      Datum:          04.01.2022
##
##      Licence:        GPLv3
##
##      $Id: $
##
##################################################################################################
## set -x

###########################################
## ANSICOLORS 
###########################################
NORMAL='\e[0;39m'
RED='\e[1;31m'
GREEN='\e[1;32m'
YELLOW='\e[1;33m'
BLUE='\e[1;34m'
MAGENTA='\e[1;35m'
CYAN='\e[1;36m'
WHITE='\e[1;37m'
BLACK='\e[30;40m'
GREY='\e[30;40m'

###########################################
## MESSAGE FUNCTIONS 
###########################################
function msg_error () {
        MSG1="$1"
        MSG2="$2"

        MSG1=$( echo "${MSG1}" | tr '[:lower:]' '[:upper:]' )

        if [ "${MSG1}" ]; then
                if [ ! "${MSG2}" ]; then
                        MESSAGE="${RED} >> ${WHITE}${MSG1}${NORMAL}"
                else
                        MESSAGE="${RED} >> ${WHITE}${MSG1} ${YELLOW}[${GREY} ${MSG2} ${YELLOW}]${NORMAL}"
                fi

		echo -e "${MESSAGE}"
        fi
}

function msg_ok () {
        MSG1="$1"
        MSG2="$2"
        MSG3="$3"

        if [ "${MSG1}" ]; then
                if [ ! "${MSG2}" ]; then
                        MESSAGE="${GREEN} >> ${WHITE}${MSG1}${NORMAL}"
                elif [ "${MSG2}" ] && [ ! "${MSG3}" ]; then
                        MESSAGE="${GREEN} >> ${WHITE}${MSG1} ${GREEN}[${GREY} ${MSG2} ${GREEN}]${NORMAL}"
                else
                        MESSAGE="${GREEN} >> ${WHITE}${MSG1} ${GREEN}[${GREY} ${MSG2} ${GREEN}]${WHITE} ${MSG3}${NORMAL}"
                fi
		echo -e "${MESSAGE}"
        fi
}

###########################################
## UPLOAD NEXTCLOUD 
###########################################
function pushnc () {

	############################################
	## IMPORT PUSHNC CONFIG
	############################################
	if [ -f /etc/pushnc/pushnc.conf.sh ]; then 
	       	. /etc/pushnc/pushnc.conf.sh
	else
		msg_error "CONFIG FILE MISSING" "/etc/pushnc/pushnc.conf.sh"
		exit 1
	fi

	NC_FILE="$1"  ## file.jpg
	if [ ! "${NC_FILE}" ]; then
		msg_error "FILE " "MISSING"
		msg_error "USAGE" "pushnc /path/to/file.name"
	else

		DL_PATH="${NC_FILE%/*}"  ## source/dir
		if [ "${NC_FILE}" = "${DL_PATH}" ]; then
			DL_PATH="."
		fi

		SUB_PATH="pushnc" ## photo

		if [ "${NC_FILE}" ] && [ ! "${NC_FILE}" = "null"  ]; then
			NC_FILE=${NC_FILE##*/}

			curl -s -q -k -u ${NC_USR}:${NC_PWD} -X MKCOL "${WEBDAV_URL}/${SUB_PATH}" &>/dev/null
			curl -s -q -# -X PUT -k -u ${NC_USR}:${NC_PWD} \
				--cookie "XDEBUG_SESSION=MROW4A;path=/;" \
				--data-binary @"${DL_PATH}/${NC_FILE}" "${WEBDAV_URL}/${SUB_PATH}/${NC_FILE}" &>/dev/null

			############################################
			## CREATE LINK
			## PARSE RESPONSE FROM NC TO EXTRACT URL
			############################################
			LINK_EXPIRE=$( date -I -d "${EXPIRE_DAYS:-0} day")
			LINK_URL_CLOUD_API=${LINK_URL}/${CLOUD_API}

			############################################
			## GET CLEARWEB LINK
			############################################
			SHARE_LINK=$( curl -s -q -k -u "${NC_USR}:${NC_PWD}" \
				-H "OCS-APIRequest: true" \
				-X POST ${LINK_URL_CLOUD_API} \
				-d path="/${SUB_PATH}/${NC_FILE}" \
				-d shareType=3 \
				-d permissions=1 \
				-d expireDate="${LINK_EXPIRE}" \
				-d tags="deleteme" | grep '^\ *<url>' | sed s/\<*.url\>//g | tr -d ' ')

			############################################
			## ONLY IF WE USE TOR SO TOR_LINK HAS TO BE 
			## SET IN YAML - FILE
			## get_tor "${CHAT_ID}"
			############################################
			if [ "${LINK_TOR}" ] && [ "${TOR}" = "1" ]; then
				SHARE_LINK=$( echo ${SHARE_LINK} | cut -d '/' -f4- )
				if [ "${SHARE_LINK}" ]; then 
					## SHARE_LINK=${LINK_TOR}/${SHARE_LINK}/download
					msg_ok "LINK   " "${SHARE_LINK}" 
					msg_ok "EXPIRES" "${LINK_EXPIRE}"
				else
					msg_error "FAILED TO UPLOAD ${SUB_PATH} FILE" "${NC_FILE}"
				fi
			else
				############################################
				## SHOW CLOUD INTERFACE ONLY IF LINK 
				## IS A CLEARWEB LINK
				############################################
				if [ "${SHARE_LINK}" ]; then
					## SHARE_LINK=${SHARE_LINK}
					msg_ok "LINK   " "${SHARE_LINK}" 
					msg_ok "EXPIRES" "${LINK_EXPIRE}"
				else
					msg_error "FAILED TO UPLOAD ${SUB_PATH} FILE" "${NC_FILE}"
				fi
			fi
		fi
	fi
}


###########################################
## DOWNLOAD NEXTCLOUD 
###########################################
pullnc () {

	DL_URL="$1"
	REGEX='(https?)://[-[:alnum:]\+&@#/%?=~_|!:,.;]+'

	if [ ! "${DL_URL}" ]; then
		msg_error "DOWNLOAD URL" "MISSING"
		msg_error "USAGE" "pullnc <nextcloud-download-url>"
		exit 1

	elif [[ ${DL_URL} =~ ${REGEX} ]]; then
		## GET FILENAME
		FILE=$( curl -q -s ${DL_URL} | grep downloadURL | awk 'BEGIN{value};{print $4 }' | sed 's/value\=//g; s/\"//g' )
		FILE="$( echo ${FILE##*/} )"

		############################################
		## DOWNLOAD FILE
		############################################
		curl -q -s ${DL_URL}/download -o ${FILE}
		if [ $? -eq 0 ]; then
			msg_ok "FILE SUCCESSFULLY DOWNLOADED" "${FILE}"
		else
			msg_error "FILED TO DOWNLOAD FILE" "${FILE}"
			exit 1
		fi
	else
		msg_error "NOT A VALID NEXTCLOUD URL" "${DL_URL}"
		exit 1
	fi
}
