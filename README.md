> # pushnc

## Push and pull file to and from nextcloud
This script is designed to easily push or pull a files to an nextcloud instance. 
> push files ( pushnc ) sends a file to an configurend nextcloud instance. 
> If upload was successfull, you will get a downloadlink.

### UPLOAD FILE

> **pushnc** /path/to/file.xyz 
### DOWNLOAD FILE
> **pullnc** https://your.nextcloud.domain/s/kasjhkjasdhfkjhdskjf 

## In order to use this script
- copy pushnc.sh to /etc/profiles.d/pushnc.sh
- create directory /etc/pushnc/
- copy config/pushnc.conf.sh.sample to /etc/pushnc/pushnc.conf.sh
- edit /etc/pushnc/pushnc.conf.sh and set your values



## Installation

```
#!/bin/bash

cp pushnc.sh /etc/profile.d/
mkdir -p /etc/pushnc/

cp config/pushnc.conf.sh.sample /etc/pushnc/
if [ ! -f /etc/pushnc/pushnc.conf.sh ]; then
    cp config/pushnc.conf.sh.sample /etc/pushnc/pushnc.conf.sh
fi

echo "INSTALLATION HAS FINISHED"
echo "EXECUTE :: . /etc/profile.d/pushnc.sh"
```



The easiest way to activate ist to logoff and logon again or source the script with the following command.

`
. /etc/profiles.d/pushnc.sh
`

After installation and after logoff an logon you will have two new commands

- pushnc
- pullnc

## /etc/pushnc/pushnc.conf.sh
```
##########################################################
## SAMPLE CONFIGURATION FILE 
## -------------------------
## SET YOUR VALUES AND COPY IT TO:
## /etc/pushnc/pushnc.conf.sh
##########################################################

## URL FOR UPLOADING FILES
WEBDAV_URL="https://your.domain.sample/remote.php/dav/files/<username>"

## URL FOR LINK CREATION USING API
LINK_URL="https://your.domain.sample"

## ONIONADDRESS OF THE ABOVE LINK_URL INSTANCE IF YOU HAVE ONE
## IF NOT SET, LINK_URL IS USED
LINK_TOR=""

## NEXTCLOUDC API URL
CLOUD_API="ocs/v1.php/apps/files_sharing/api/v1/shares"

## NEXTCLOUD CREDENTIALS
NC_USR="nc-username"
NC_PWD="nc-app-token"

## WHEN THE LINK WILL EXPIRE
EXPIRE_DAYS="10"
```
