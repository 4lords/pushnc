#!/bin/bash

cp pushnc.sh /etc/profile.d/
mkdir -p /etc/pushnc/

cp config/pushnc.conf.sh.sample /etc/pushnc/
if [ ! -f /etc/pushnc/pushnc.conf.sh ]; then
    cp config/pushnc.conf.sh.sample /etc/pushnc/pushnc.conf.sh
fi

echo "INSTALLATION HAS FINISHED"
echo "EXECUTE :: . /etc/profile.d/pushnc.sh"
